package com.example.user.arv;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EdgeEffect;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class Fragement_settings extends Fragment {
    TextView name, email, phone, dob, address, KYC;
    String s_name, s_email, s_phone, s_dob, s_address;
    Button bt,settings,wallet;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_settings, container, false);

        settings = (Button) v.findViewById(R.id.settingss);
        wallet = (Button)v.findViewById(R.id.wallet);
        name=(TextView)v.findViewById(R.id.prof_name);
        email=(TextView)v.findViewById(R.id.Prof_email);
        phone=(TextView)v.findViewById(R.id.prof_phone);
        bt = (Button)v.findViewById(R.id.signout);
        address=(TextView)v.findViewById(R.id.prof_address);
        dob=(TextView)v.findViewById(R.id.prof_dob);
        KYC = (TextView)v.findViewById(R.id.add_proff);

        idproof_check();


        if ((KYC.getText().toString()).equals("Approved")){
            KYC.setClickable(false);
        }

        KYC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),chooseimage.class));
            }
        });

        wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              final Dialog d = new Dialog(getActivity());
              d.setContentView(R.layout.dialog_wallet);
              Button bt = (Button)d.findViewById(R.id.walletbalance);
              Button addcardbt = (Button)d.findViewById(R.id.addcard);
              d.show();
              bt.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                      sessionData obj = new sessionData();
                      get_wallet(obj.email,d);
                  }
              });

              addcardbt.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                      
                  }
              });
            }
        });


        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog d = new Dialog(getActivity());

                d.setContentView(R.layout.dialog_settings);
                Button n = (Button)d.findViewById(R.id.cname);
                Button pass = (Button)d.findViewById(R.id.cpassword);
                Button dob = (Button)d.findViewById(R.id.cdob) ;
                Button a = (Button)d.findViewById(R.id.caddress);
                Button p = (Button)d.findViewById(R.id.cphone);
                d.show();


                final Dialog conf = new Dialog(getActivity());
                conf.setContentView(R.layout.dialog_edit);
                final TextView t = (TextView)conf.findViewById(R.id.text) ;
                final EditText ed = (EditText)conf.findViewById(R.id.value);
                final Button bcon = (Button)conf.findViewById(R.id.confirm);


                pass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        d.dismiss();
                        final Dialog ch_dialog = new Dialog(getActivity());
                        ch_dialog.setContentView(R.layout.dialog_changepassword);
                        final EditText password = (EditText)ch_dialog.findViewById(R.id.currentpass) ;
                        final EditText newpassword = (EditText)ch_dialog.findViewById(R.id.newpass) ;
                        Button bt = (Button)ch_dialog.findViewById(R.id.confirm);
                        ch_dialog.show();
                        bt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String nps = newpassword.getText().toString();
                                String ps = password.getText().toString();
                                sessionData obj = new sessionData();
                                String email = obj.email;
                                edit_password(nps,ps,email,ch_dialog);

                            }
                        });
                    }
                });

                n.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        d.dismiss();
                      t.setText("Enter the new name");
                      conf.show();
                      bcon.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View view) {
                              String x = ed.getText().toString();
                              String param = "name";
                              change(param,x);
                              conf.dismiss();
                          }
                      });

                    }
                });

                dob.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        d.dismiss();
                        t.setText("Enter the new name");
                        conf.show();
                        bcon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String x = ed.getText().toString();
                                String param = "dob";
                                change(param,x);
                                conf.dismiss();
                            }
                        });


                    }
                });

                a.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        d.dismiss();
                        t.setText("Enter the new Address");
                        conf.show();
                        bcon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String x = ed.getText().toString();
                                String param = "address";
                                change(param,x);
                                conf.dismiss();
                            }
                        });

                    }
                });

                p.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        d.dismiss();
                        t.setText("Enter the new Phone Number");
                        conf.show();
                        bcon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String x = ed.getText().toString();
                                String param = "phone";
                                change(param,x);
                                conf.dismiss();
                            }
                        });

                    }
                });

            }
        });

        sessionData s = new sessionData();
        String m = s.info;
        try {
            JSONObject j = new JSONObject(m);
            s_name = j.getString("name");
            name.setText(s_name);
            s_email = j.getString("email");
            email.setText(s_email);
            s_phone = j.getString("phone");
            phone.setText(s_phone);
            s_dob = j.getString("dob");
            dob.setText(s_dob);
            s_address = j.getString("address");
            address.setText(s_address);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preff = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = preff.edit();
                ed.putString("logged_in","na");
                ed.putString("email","na");
                ed.putString("password","na");
                ed.commit();


                startActivity(new Intent(getActivity(),MainActivity.class));
                getActivity().finish();
            }
        });

        return  v;












    }

public void change(final String param, final String value){
    AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
    client.setTimeout(8000);
    RequestParams params = new RequestParams();
    client.setURLEncodingEnabled(true);
sessionData obj = new sessionData();


    client.post(getActivity(), "https://muffy.pythonanywhere.com/edit?param="+param+"&item="+value+"&email="+obj.email,
            params, new AsyncHttpResponseHandler() {




                public void onStart() {

                    super.onStart();


                }

                @Override
                public void onSuccess(int arg0,
                                      Header[] arg1, byte[] arg2) {


                    Toast.makeText(getActivity(),new String(arg2),Toast.LENGTH_LONG).show();
                    if (param.equals("name")){
                        name.setText(value);
                    }
                    if (param.equals("phone")){
                        phone.setText(value);
                    }
                    if (param.equals("dob")){
                        dob.setText(value);
                    }

                    if (param.equals("address")){
                        address.setText(value);
                    }



                }

                @Override
                public void onFailure(int arg0,
                                      Header[] arg1, byte[] arg2,
                                      Throwable arg3) {
                    Toast.makeText(getActivity(),new String(arg2),Toast.LENGTH_LONG).show();



                }

                public void onFinish() {
                    super.onFinish();




                }
            });


}


    public void edit_password(final String newps, String ps, String email, final Dialog d){
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setTimeout(8000);
        RequestParams params = new RequestParams();
        client.setURLEncodingEnabled(true);



        client.post(getActivity(), "https://muffy.pythonanywhere.com/edit_password?currentpassword="+ps+"&newpassword="+newps+"&email="+email,
                params, new AsyncHttpResponseHandler() {




                    public void onStart() {

                        super.onStart();


                    }

                    @Override
                    public void onSuccess(int arg0,
                                          Header[] arg1, byte[] arg2) {


                        Toast.makeText(getActivity(),new String(arg2),Toast.LENGTH_LONG).show();
                        SharedPreferences preff = getActivity().getSharedPreferences("data",Context.MODE_PRIVATE) ;
                        SharedPreferences.Editor ed = preff.edit();
                        ed.putString("password",newps);
                        ed.commit();
                        d.dismiss();
                    }

                    @Override
                    public void onFailure(int arg0,
                                          Header[] arg1, byte[] arg2,
                                          Throwable arg3) {
                        Toast.makeText(getActivity(),new String(arg2),Toast.LENGTH_LONG).show();
                        d.dismiss();


                    }

                    public void onFinish() {
                        super.onFinish();




                    }
                });


    }


    public void get_wallet(final String email, final Dialog d){
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setTimeout(8000);
        RequestParams params = new RequestParams();
        client.setURLEncodingEnabled(true);



        client.post(getActivity(), "https://muffy.pythonanywhere.com/get_wallet?email="+email,
                params, new AsyncHttpResponseHandler() {




                    public void onStart() {

                        super.onStart();


                    }

                    @Override
                    public void onSuccess(int arg0,
                                          Header[] arg1, byte[] arg2) {


                        d.dismiss();
                        Dialog bal = new Dialog(getActivity());
                        bal.setContentView(R.layout.dialog_text);
                        TextView t = (TextView)bal.findViewById(R.id.text);
                        t.setText("Your Current Wallet Balance is :\n"+new String(arg2));
                        bal.show();


                    }

                    @Override
                    public void onFailure(int arg0,
                                          Header[] arg1, byte[] arg2,
                                          Throwable arg3) {
                        Toast.makeText(getActivity(),new String(arg2),Toast.LENGTH_LONG).show();
                        d.dismiss();


                    }

                    public void onFinish() {
                        super.onFinish();




                    }
                });


    }


    public void idproof_check(){

        SharedPreferences preff = getActivity().getSharedPreferences("data",Context.MODE_PRIVATE) ;
        SharedPreferences.Editor ed = preff.edit();
        if (preff.contains("id_proof_exists")){

            Boolean b = preff.getBoolean("id_proof_exists",true);
            if (b){
                KYC.setText("Approved");
            }else{
                KYC.setText("click here to upload id proof");
                KYC.setTextColor(Color.parseColor("#ec3135"));
            }

        }else{

            ed.putBoolean("id_proof_exists",false);
            ed.commit();
            KYC.setText("click here by to upload id proof");
            KYC.setTextColor(Color.parseColor("#ec3135"));
        }

    }



}
