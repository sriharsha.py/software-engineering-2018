package com.example.user.arv;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by User on 2/27/2018.
 */

public class Fragment_list extends Fragment {
    private GridLayoutManager lLayout;
    LinearLayout lay;

    ArrayList<String> names,ratings,langs;
    ArrayList<Bitmap> imgs;
    Bitmap[] im;
    private static final int CONTACTS = 0;
    private static final int COUNTRIES = 1;
    private static final Random RANDOM = new Random();
    private RecyclerView recyclerView;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_list, container, false);
        names = new ArrayList<String>();
        ratings = new ArrayList<String>();
        langs = new ArrayList<String>();
        imgs = new ArrayList<Bitmap>();

        sessionData obj = new sessionData();
        String s = obj.movieslist ;
        imgs = obj.MOVIE_IMAGES;
        getapiData obj2 = new getapiData();
        im = obj2.I;
        sort(s);

        lLayout = new GridLayoutManager(v.getContext(), 1);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        setupRecyclerView();



        return v;
    }




    private void setupRecyclerView() {
        recyclerView.setLayoutManager(lLayout);
        setContactsAdapter(names,langs,ratings,im);

    }

    private void setContactsAdapter(ArrayList<String> array, ArrayList<String> array2, ArrayList<String> array3, Bitmap[] array4) {
        recyclerView.setAdapter(
                new SimpleStringRecyclerViewAdapter(getActivity(), array, array2, array3,array4, CONTACTS));
    }


    public static class SimpleStringRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder> {

        private final TypedValue mTypedValue = new TypedValue();
        private int mBackground;
        private List<String> mValues;
        private List<String> mValues2;
        private List<String> mValues3;
        private Bitmap[] mValues4;
        private int[] mMaterialColors;
        private int mType;

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public String mBoundString;

            public final View mView;
            public final ImageView img;
            public final TextView mTextView;
            public final TextView mTextView2;
            public final TextView mTextView3;
            public final Button b;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                img = (ImageView) view.findViewById(R.id.movie_image);
                mTextView = (TextView) view.findViewById(R.id.movie_name);
                mTextView2 = (TextView) view.findViewById(R.id.movie_lang);
                mTextView3 = (TextView) view.findViewById(R.id.movie_rating);
                b = (Button)view.findViewById(R.id.book_ticket);
            }

            @Override public String toString() {
                return super.toString() + " '" + mTextView.getText();
            }
        }

        public String getValueAt(int position) {
            return mValues.get(position);
        }

        public SimpleStringRecyclerViewAdapter(Context context, List<String> items, List<String> items2,List<String> items3,Bitmap[] items4,int type) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);

            mBackground = mTypedValue.resourceId;
            mValues = items;
            mValues2 = items2;
            mValues3= items3;
            mValues4 = items4;
            mType = type;
        }

        @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view =
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);

            view.setBackgroundResource(mBackground);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                (view.findViewById(R.id.lis)).setElevation(10);
            }

            return new ViewHolder(view);
        }

        @Override public void onBindViewHolder(final ViewHolder holder, int position) {

            holder.mBoundString = mValues.get(position);

            holder.mTextView.setText(mValues.get(position));
            holder.mTextView2.setText(mValues2.get(position));
            holder.mTextView3.setText(mValues3.get(position));
            holder.img.setImageBitmap(mValues4[position]);

        }

        @Override public int getItemCount() {
            return mValues.size();
        }
    }



    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }







public void sort(String s){

    try {
        JSONArray j = new JSONArray(s);
        for (int i = 0 ;i <j.length();i++){
            JSONObject js = j.getJSONObject(i);
            names.add(js.getString("movie_name"));
            langs.add(js.getString("language"));
            ratings.add(js.getString("rating")+"%");
        }


    } catch (JSONException e) {
        e.printStackTrace();
    }


}

}
