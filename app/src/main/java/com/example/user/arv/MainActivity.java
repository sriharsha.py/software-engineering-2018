package com.example.user.arv;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {
EditText username;
EditText password;
TextView login;
Button bt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


if (login_status()){

  postit2();

}



        setContentView(R.layout.activity_main);





        username = (EditText)findViewById(R.id.username);


        password =(EditText)findViewById(R.id.password);
        login= (TextView)findViewById(R.id.register);
        username.setBackgroundColor(Color.argb(70, 155, 155, 155));
        password.setBackgroundColor(Color.argb(70, 155, 155, 155));
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Register.class));

            }
        });
        bt = (Button)findViewById(R.id.button);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkifempty()){

                    postit();


                }



            }
        });

    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void postit(){

        final Dialog d = new Dialog(MainActivity.this);
        d.setContentView(R.layout.dialog_text);
        TextView t = (TextView)d.findViewById(R.id.text);
        t.setText("Processing Please Wait...");
        d.show();



        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setTimeout(8000);
        RequestParams params = new RequestParams();
        client.setURLEncodingEnabled(true);


        client.post(MainActivity.this, "https://muffy.pythonanywhere.com/signin?email="+username.getText().toString().trim()+"&password="+password.getText().toString().trim(),
                params, new AsyncHttpResponseHandler() {




                    public void onStart() {

                        super.onStart();


                    }

                    @Override
                    public void onSuccess(int arg0,
                                          Header[] arg1, byte[] arg2) {

                        try {
                            JSONObject j = new JSONObject(new String(arg2));

                            if (j.getBoolean("success")){
                                d.dismiss();
                                sessionData o = new sessionData();
                                o.email = username.getText().toString();
                                SharedPreferences preff = MainActivity.this.getSharedPreferences("data",Context.MODE_PRIVATE) ;
                                SharedPreferences.Editor ed = preff.edit();
                                ed.putString("email",username.getText().toString());
                                ed.putString("password",password.getText().toString());
                                ed.putString("logged_in","yes");
                                ed.commit();
                                getapiData obj = new getapiData(MainActivity.this);


                            }else{

                                Toast.makeText(MainActivity.this,"Incorrect cred",Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

        @Override
        public void onFailure(int arg0,
        Header[] arg1, byte[] arg2,
        Throwable arg3) {
           Toast.makeText(MainActivity.this,new String(arg2),Toast.LENGTH_LONG).show();



        }

    public void onFinish() {
        super.onFinish();




    }
});


        }


    public void postit2(){

        final Dialog d = new Dialog(MainActivity.this);
        d.setContentView(R.layout.dialog_text);
        TextView t = (TextView)d.findViewById(R.id.text);
        t.setText("Processing Please Wait...");
        d.show();
        SharedPreferences preff = MainActivity.this.getSharedPreferences("data",Context.MODE_PRIVATE) ;
        String em = preff.getString("email","");
        String ps = preff.getString("password","");
        sessionData obj = new sessionData();
        obj.email = em;
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setTimeout(8000);
        RequestParams params = new RequestParams();
        client.setURLEncodingEnabled(true);


        client.post(MainActivity.this, "https://muffy.pythonanywhere.com/signin?email="+em+"&password="+ps,
                params, new AsyncHttpResponseHandler() {




                    public void onStart() {

                        super.onStart();


                    }

                    @Override
                    public void onSuccess(int arg0,
                                          Header[] arg1, byte[] arg2) {

                        try {
                            JSONObject j = new JSONObject(new String(arg2));

                            if (j.getBoolean("success")){
                                d.dismiss();

                                getapiData obj = new getapiData(MainActivity.this);


                            }else{

                                Toast.makeText(MainActivity.this,"Incorrect cred",Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int arg0,
                                          Header[] arg1, byte[] arg2,
                                          Throwable arg3) {
                        Toast.makeText(MainActivity.this,new String(arg2),Toast.LENGTH_LONG).show();



                    }

                    public void onFinish() {
                        super.onFinish();




                    }
                });


    }


        public boolean checkifempty(){

        String s1 = username.getText().toString().trim();
        String p1 = password.getText().toString().trim();

        if (!isNetworkAvailable()){
            Toast.makeText(MainActivity.this,"No internet connection....",Toast.LENGTH_LONG).show();
            return false;

        }

        if (TextUtils.isEmpty(s1)){
            username.setError("This field cannot be empty....");
            return false;
        }
        if (TextUtils.isEmpty(p1)){
            password.setError("This field cannot be empty....");
            return false;
        }

        return true;
        }



public boolean login_status(){
    SharedPreferences preff = MainActivity.this.getSharedPreferences("data",Context.MODE_PRIVATE);
    if (preff.contains("email")){
        String e = preff.getString("email","na");
        if (!e.equals("na")){
            String status = preff.getString("logged_in","");
            if (status.equals("yes")){
                return true;
            }else{
                return false;
            }
        }
    }
    return false;
}

}
