package com.example.user.arv;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;

public class Register extends AppCompatActivity {
    EditText name;
    EditText email;
    EditText pswd;
    EditText pswd1;
    EditText num,dob,add;
    Button bt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        email = (EditText) findViewById(R.id.Email);
        name = (EditText) findViewById(R.id.Name);
        pswd = (EditText) findViewById(R.id.pswd);
        num = (EditText) findViewById(R.id.phn);
        pswd1 = (EditText) findViewById(R.id.pswd1);
        bt = (Button)findViewById(R.id.button);
        dob = (EditText)findViewById(R.id.dob);
        add = (EditText)findViewById(R.id.address) ;



        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkifempty() && emailValidation() && password() && phonenumValidation()&&password() ){
                    postit();
                           // startActivity(new Intent(getApplicationContext(),EmailVerification.class));






                }

            }
        });

    }




    public boolean checkifempty(){



        if (!isNetworkAvailable()){
            Toast.makeText(Register.this,"No internet connection....",Toast.LENGTH_LONG).show();
            return false;

        }

        String s1 = name.getText().toString().trim();




        if (TextUtils.isEmpty(s1)){
            name.setError("This field cannot be empty....");
            return false;
        }


        return true;
    }


    public boolean password() {
        String a1 = pswd.getText().toString().trim();
        String a2 = pswd1.getText().toString().trim();




        if(TextUtils.isEmpty(a1)){
            pswd.setError("This field cant be empty.. .!!");
            return false;

        }

        if(a1.equals(a2) ){
            return true;
        }else{
            pswd1.setError("passwords doesnt match .!!");
            return false;
        }
    }

    public boolean phonenumValidation() {


        String regexStr = "^[0-9]{10}$";
        Pattern p = Pattern.compile(regexStr);
        Matcher check = p.matcher(num.getText().toString());
        Boolean b = check.matches();
        if (!b){
            num.setError("Enter valid phone number.!!");
            return false;
        }else{

            return  true;
        }


    }

    public boolean emailValidation(){
        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern p = Pattern.compile(EMAIL_STRING);
        Matcher check = p.matcher(email.getText().toString());

        Boolean b = check.matches();
        if (!b){
            email.setError("Enter valid email address..!!");
            return false;
        }else{

            return  true;
        }



    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



    public void postit(){






        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setTimeout(8000);
        RequestParams params = new RequestParams();
        client.setURLEncodingEnabled(true);

       String Email= email.getText().toString().trim();
        String Pswd= pswd.getText().toString().trim();
        String Num= num.getText().toString().trim();
        String Name=name.getText().toString().trim();





        client.post(Register.this, "https://muffy.pythonanywhere.com/signup?email="+Email+"&password="+Pswd+"&name="+Name+"&phone="+Num+"%dob="+dob.getText().toString()+"&address="+add.getText().toString(),
                params, new AsyncHttpResponseHandler() {




                    public void onStart() {

                        super.onStart();


                    }

                    @Override
                    public void onSuccess(int arg0,
                                          Header[] arg1, byte[] arg2) {

                        try {
                            JSONObject j = new JSONObject(new String(arg2));

                            if (j.getBoolean("success")){
                                startActivity(new Intent(Register.this,MainActivity.class));
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        Toast.makeText(Register.this,new String(arg2),Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onFailure(int arg0,
                                          Header[] arg1, byte[] arg2,
                                          Throwable arg3) {

                        Toast.makeText(Register.this,new String(arg2),Toast.LENGTH_LONG).show();


                    }

                    public void onFinish() {
                        super.onFinish();




                    }
                });


    }






}