package com.example.user.arv;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import java.io.File;

public class chooseimage extends AppCompatActivity {
    private static int RESULT_LOAD_IMAGE = 1;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chooseimage);
         imageView = (ImageView) findViewById(R.id.imgView);

        Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(intent, 2);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Uri selectedImage = data.getData();

        String[] filePath = { MediaStore.Images.Media.DATA };

        Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);

        c.moveToFirst();

        int columnIndex = c.getColumnIndex(filePath[0]);

        String picturePath = c.getString(columnIndex);

        c.close();

        Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));



        imageView.setImageBitmap(thumbnail);


    }


}
