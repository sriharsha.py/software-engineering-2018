package com.example.user.arv.font_res;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by harsha on 12/25/17.
 */


public class AbelRegular extends android.support.v7.widget.AppCompatTextView{
    public AbelRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public AbelRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AbelRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/AbelRegular.ttf");
        setTypeface(tf ,1);

    }

}
