package com.example.user.arv.font_res;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Dmytro Denysenko on 5/6/15.
 */
@SuppressLint("AppCompatCustomView")
public class CanaroTextView extends TextView {

    public CanaroTextView(Context context, AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);
        init();

    }

    public CanaroTextView(Context context, AttributeSet attrs) {

        super(context, attrs);
        init();

    }

    public CanaroTextView(Context context) {

        super(context);
        init();

    }

    public void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/canaro_extra_bold.otf");
        setTypeface(tf ,1);

    }


}
