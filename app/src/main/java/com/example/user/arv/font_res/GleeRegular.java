package com.example.user.arv.font_res;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by harsha on 12/21/17.
 */
@SuppressLint("AppCompatCustomView")
 class GleeRegular extends TextView {

    public GleeRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public GleeRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GleeRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Glegoo-Regular.ttf");
        setTypeface(tf ,1);

    }
}
