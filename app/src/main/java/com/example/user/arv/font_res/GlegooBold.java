package com.example.user.arv.font_res;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by harsha on 12/21/17.
 */
@SuppressLint("AppCompatCustomView")
public class GlegooBold extends TextView {

    public GlegooBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public GlegooBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GlegooBold(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/GlegooBold.ttf");
        setTypeface(tf ,1);

    }
}
