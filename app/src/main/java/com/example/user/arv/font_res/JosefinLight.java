package com.example.user.arv.font_res;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import static java.security.AccessController.getContext;

/**
 * Created by harsha on 1/3/18.
 */
@SuppressLint("AppCompatCustomView")
public class JosefinLight extends TextView {

    public JosefinLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public JosefinLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public JosefinLight(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/JosefinSans-Light.ttf");
        setTypeface(tf ,1);

    }


}
