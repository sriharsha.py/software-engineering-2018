package com.example.user.arv.font_res;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by harsha on 1/3/18.
 */

public class JosefinRegular extends android.support.v7.widget.AppCompatTextView {


    public JosefinRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public JosefinRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public JosefinRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/JosefinSans-Regular.ttf");
        setTypeface(tf ,1);

    }
}
