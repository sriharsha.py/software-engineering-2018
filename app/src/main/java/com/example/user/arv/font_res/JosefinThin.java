package com.example.user.arv.font_res;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by harsha on 1/3/18.
 */
@SuppressLint("AppCompatCustomView")
public class JosefinThin extends TextView {

    public JosefinThin(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public JosefinThin(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public JosefinThin(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/JosefinSans-Thin.ttf");
        setTypeface(tf ,1);

    }

}
