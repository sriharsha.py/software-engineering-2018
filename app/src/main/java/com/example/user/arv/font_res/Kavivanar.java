package com.example.user.arv.font_res;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by harsha on 1/19/18.
 */
@SuppressLint("AppCompatCustomView")
public class Kavivanar extends TextView {

    public Kavivanar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Kavivanar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Kavivanar(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Kavivanar-Regular.ttf");
        setTypeface(tf ,1);

    }
}
