package com.example.user.arv.font_res;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by harsha on 1/21/18.
 */
@SuppressLint("AppCompatCustomView")
public class spincycle extends TextView {


    public spincycle(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public spincycle(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public spincycle(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/SPINC___.TTF");
        setTypeface(tf ,1);

    }
}
