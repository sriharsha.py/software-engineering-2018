package com.example.user.arv;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import cz.msebera.android.httpclient.Header;

/**
 * Created by User on 3/15/2018.
 */

public class getapiData {
Bitmap bx;
    public  static Bitmap[] I;
    Boolean[] checkpoints;
    TextView t;
    Dialog d;

    public getapiData(Context context){
if (isNetworkAvailable(context)){
    getname(context);
    getMoviesList(context);


}


    }
    public getapiData(){}




    public void getMoviesList(final Context context){
       d = new Dialog(context);
       d.setContentView(R.layout.dialog_text);
       d.setCancelable(false);
       d.show();

        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setTimeout(8000);
        RequestParams params = new RequestParams();
        client.setURLEncodingEnabled(true);


        client.get(context, "https://muffy.pythonanywhere.com/movieslist",
                params, new AsyncHttpResponseHandler() {




                    public void onStart() {

                        super.onStart();


                    }

                    @Override
                    public void onSuccess(int arg0,
                                          Header[] arg1, byte[] arg2) {

                       String s = new String(arg2);

                       sessionData obj = new sessionData();
                       obj.movieslist = s;
                         t = (TextView)d.findViewById(R.id.text);
                        t.setText("Loading Bundles....");
                        getImages(context);
                    }

                    @Override
                    public void onFailure(int arg0,
                                          Header[] arg1, byte[] arg2,
                                          Throwable arg3) {
                        Toast.makeText(context,new String(arg2),Toast.LENGTH_LONG).show();



                    }

                    public void onFinish() {
                        super.onFinish();




                    }
                });


    }



    public void getname(final Context context){



        d = new Dialog(context);
        d.setContentView(R.layout.dialog_text);
        d.setCancelable(false);
        d.show();
        String email;
        sessionData obj = new sessionData();
        email=obj.email;


        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setTimeout(8000);
        RequestParams params = new RequestParams();
        client.setURLEncodingEnabled(true);


        client.post(context, "https://muffy.pythonanywhere.com/getuserinfo?email="+email,
                params, new AsyncHttpResponseHandler() {




                    public void onStart() {

                        super.onStart();


                    }

                    @Override
                    public void onSuccess(int arg0,
                                          Header[] arg1, byte[] arg2) {

                        String s = new String(arg2);

                        sessionData obj = new sessionData();
                        obj.info = s;

                    }

                    @Override
                    public void onFailure(int arg0,
                                          Header[] arg1, byte[] arg2,
                                          Throwable arg3) {
                        Toast.makeText(context,new String(arg2),Toast.LENGTH_LONG).show();



                    }

                    public void onFinish() {
                        super.onFinish();




                    }
                });


    }



    public void getImages(Context context){


        sessionData obj = new sessionData();
        String s = obj.movieslist;

        try {
            JSONArray j = new JSONArray(s);
            I = new Bitmap[j.length()];
            checkpoints = new Boolean[j.length()];

            for (int z =0; z < checkpoints.length; z++){
                checkpoints[z] = false;
            }

            for (int i = 0 ; i<j.length(); i++){
                JSONObject js = j.getJSONObject(i);
                String url = js.getString("img_url");

                 image_APICALL(context,url,i,j.length());


            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public void image_APICALL(final Context context, String url, final int i, final int size){

        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setTimeout(8000);
        RequestParams params = new RequestParams();
        client.setURLEncodingEnabled(true);


        client.get(context, url,
                params, new AsyncHttpResponseHandler() {




                    public void onStart() {

                        super.onStart();


                    }

                    @Override
                    public void onSuccess(int arg0,
                                          Header[] arg1, byte[] arg2) {

                        Bitmap b = BitmapFactory.decodeByteArray(arg2,0,arg2.length);
                        I[i] = b;
                        t.setText("Loading Index " + i);
                        checkpoints[i] = true;
                        Boolean flag = true;
                       for (int m = 0 ; m<checkpoints.length;m++){
                           if (!checkpoints[m]){
                               flag = false;
                               break;
                           }
                       }


                       if (flag){


                           d.dismiss();
                          context.startActivity(new Intent(context,movieslist.class));

                       }
                    }

                    @Override
                    public void onFailure(int arg0,
                                          Header[] arg1, byte[] arg2,
                                          Throwable arg3) {




                    }

                    public void onFinish() {
                        super.onFinish();




                    }
                });



    }


    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
